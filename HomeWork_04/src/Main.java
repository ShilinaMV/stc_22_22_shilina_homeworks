import java.util.Scanner;

public class Main {
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        for (int i = from-1; i <= to-1; i++) {
            sum += a[i];
        }
        return sum;
    }

    public static void printNumbersIsEven(int[] array) {
        for (int number : array)
            if (number % 2 == 0)
                System.out.println(number);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива:");
        int length = scanner.nextInt();
        int[] array = new int[length];
        if (length == 0) {
            System.out.println("Массив должен содержать элементы!");
            return;
        }
        System.out.println("Введите элементы массива:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Введите интервал ОТ:");
        int x = scanner.nextInt();
        System.out.println("Введите интервал ДО:");
        int y = scanner.nextInt();
        if (y < x || x > length  || y > length ) {
            System.out.println("-1");
            return;
        }
        int sum = calcSumOfArrayRange(array, x, y);
        System.out.println("Сумма: " + sum);
        System.out.println("Четные элементы массива: ");
        printNumbersIsEven(array);
    }
}