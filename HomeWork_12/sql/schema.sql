create table driver (
                        id bigserial primary key,
                        first_name char(20) not null,
                        last_name char(20) not null ,
                        phone char(20),
                        experience integer,
                        age integer check (age >=0 and age <= 120) not null,
                        drivers_license bool not null,
                        category char(10),
                        rating integer check (rating >= 0 and rating <= 5)
);

create table car (
                     id bigserial primary key,
                     model char(20) not null,
                     color char(20),
                     number char(10) not null,
                     owner_id bigint
);

create table trip (
                      driver_id bigint,
                      car_id bigint,
                      date timestamp,
                      duration char(20),
                      foreign key (driver_id) references driver(id),
                      foreign key (car_id) references car(id)
);