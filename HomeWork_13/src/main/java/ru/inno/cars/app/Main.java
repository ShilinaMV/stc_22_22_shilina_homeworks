package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarRepository;
import ru.inno.cars.repository.CarRepositoryJdbcImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;
import java.util.Scanner;


@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"-action"})
    private String action;
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        Main app = new Main();

        JCommander.newBuilder()
                .addObject(app)
                .build()
                .parse(args);

        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader
                    (new InputStreamReader(Objects.requireNonNull(Main.class.getResourceAsStream("/db.properties")))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarRepository carRepository = new CarRepositoryJdbcImpl(dataSource);

        if ("read".equals(app.action)) {
            for (Car car : carRepository.findAll()) {
                System.out.println(car);
            }
        } else if ("write".equals(app.action)) {
            while (true) {
                System.out.println("Add car information:");
                System.out.print("Model: ");
                String model = scanner.nextLine();

                System.out.print("Color: ");
                String color = scanner.nextLine();

                System.out.print("Number: ");
                String number = scanner.nextLine();

                System.out.print("Owner Id: ");
                Long owner_id = scanner.nextLong();
                scanner.nextLine();

                Car car = Car.builder()
                        .model(model)
                        .color(color)
                        .number(number)
                        .owner_id(owner_id)
                        .build();

                carRepository.save(car);

                System.out.print("Exit?(Y/N): ");
                String exit = scanner.nextLine();
                if (exit.equals("Y")) {
                    break;
                }
            }
        } else {
            System.err.println("Incorrect value of action");
        }
    }
}

