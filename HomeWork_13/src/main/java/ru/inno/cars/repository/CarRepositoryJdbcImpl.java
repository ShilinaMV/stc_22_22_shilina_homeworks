package ru.inno.cars.repository;

import ru.inno.cars.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CarRepositoryJdbcImpl implements CarRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from car order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into car (model, color, number, owner_id) " +
            "values (?, ?, ?, ?)";

    private DataSource dataSource;

    public CarRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Car> userRowMapper = row -> {
        try {
            return Car.builder()
                    .id(row.getLong("id"))
                    .model(row.getString("model"))
                    .color(row.getString("color"))
                    .number(row.getString("number"))
                    .owner_id(row.getLong("owner_id"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = userRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return cars;
    }

    @Override
    public void save(Car car) {
        // создаем объект PreparedStatement внутри try-with-resources, чтобы автоматически все закрыть
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            // кладем нужные параметры для текущего запроса из объекта cars
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getNumber());
            preparedStatement.setLong(4, car.getOwner_id());
            // выполняем запрос и получаем количество строк, которые были изменены
            int affectedRows = preparedStatement.executeUpdate();
            // если была изменена не одна строка, то выбрасываем ошибку
            if (affectedRows != 1) {
                throw new SQLException("Can't insert cars");
            }
            // получаем итератор по сгенерированным ключам в БД для текущего запроса
            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                // нужно сдвинуть итератор на первую позицию
                if (generatedId.next()) {
                    // забрать сгенерированный id и отправить его
                    car.setId(generatedId.getLong("id"));
                } else {
                    // если сдвинуть итератор не получилось, выбрасываем исключение
                    throw new SQLException("Can't obtain generated id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
