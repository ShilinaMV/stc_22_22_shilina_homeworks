public class Atm {
    private int balanceAtm;
    private final int maxAmountToBeIssued;
    private final int maxAmountAtm;
    private int numberOfOperations;

    public int getBalanceAtm() {
        return balanceAtm;
    }

    public int getNumberOfOperations() {
        return numberOfOperations;
    }

    Atm(int balanceAtm, int maxAmountToBeIssued, int maxAmountAtm) {
        this.balanceAtm = balanceAtm;
        this.maxAmountToBeIssued = maxAmountToBeIssued;
        this.maxAmountAtm = maxAmountAtm;
    }

    public int issuanceOfMoney(int amountOfMoney) {
        numberOfOperations++;
        if (amountOfMoney > maxAmountToBeIssued) {
            if (amountOfMoney > balanceAtm) {
                amountOfMoney = balanceAtm;
                balanceAtm = 0;
                return amountOfMoney;
            }
            balanceAtm -= maxAmountToBeIssued;
            return maxAmountToBeIssued;
        } else if (amountOfMoney > balanceAtm) {
            amountOfMoney = balanceAtm;
            balanceAtm = 0;
            return amountOfMoney;
        } else {
            balanceAtm -= amountOfMoney;
            return amountOfMoney;
        }
    }

    public int depositingMoney(int amountOfMoney) {
        numberOfOperations++;
        if ((amountOfMoney + balanceAtm) > maxAmountAtm) {
            int newMoney = amountOfMoney + balanceAtm - maxAmountAtm;
            balanceAtm = maxAmountAtm;
            return newMoney;
        }
        balanceAtm += amountOfMoney;
        return amountOfMoney;
    }
}
