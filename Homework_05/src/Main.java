public class Main {
    public static void main(String[] args) {
        Atm atm = new Atm(5000, 15000, 50000);
        System.out.println("Выдать деньги: " + atm.issuanceOfMoney(10));
        System.out.println("Положить деньги: " + atm.depositingMoney(4000));
        System.out.println("Количество операций: " + atm.getNumberOfOperations());
        System.out.print("Баланс: " + atm.getBalanceAtm());
    }
}

