import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //считываем размер массива с консоли
        System.out.println("Введите размер массива:");
        int length = scanner.nextInt();
        int[] array = new int[length];
        int amount = 0;
        if (length == 1) {
            System.out.println("Массив должен содержать больше одного элемента!");
            return;
        }
        //считываем элементы массива
        System.out.println("Введите элементы массива:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        //подсчет количества всех локальных минимумов в массиве
        for (int i = 0; i < array.length; i++) {

            if (i == 0 && array[i] < array[i + 1]) {
                amount++;
            } else if (i > 0 && i < array.length - 1 && array[i] < array[i - 1] && array[i] < array[i + 1]) {
                amount++;
            } else if (i == array.length - 1 && array[i] < array[i - 1]) {
                amount++;
            }
        }
        System.out.println("Количество локальных минимумов в массиве: " + amount);
    }
}