package realworld;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("list.txt");
        ProductService productService = new ProductService(productsRepository);

        System.out.println(productService.countOfAllProducts());
        System.out.println(productsRepository.findById(5));
    }
}
