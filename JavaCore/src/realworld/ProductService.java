package realworld;

public class ProductService {

    private ProductsRepository productsRepository;

    public ProductService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    public int countOfAllProducts() {
        return productsRepository.findAll().size();
    }

    public String productFindById(Integer id) {
        return String.valueOf(productsRepository.findById(id));
    }
}
