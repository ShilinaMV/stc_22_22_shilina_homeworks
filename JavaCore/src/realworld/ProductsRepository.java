package realworld;

import java.io.IOException;
import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();
    List<Product> findAllByAmountGreaterThan(Integer amount);
    //List<Product> findById(Integer id);
    //List<Product> update(Product product);

    void save(Product product);

    List<String> findAllUniqueNames();

    int findMinAmount();

    double findAverageAmount();

    Product findById(Integer id);

}
