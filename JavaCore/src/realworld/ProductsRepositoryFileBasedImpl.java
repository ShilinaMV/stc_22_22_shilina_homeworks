package realworld;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Integer amount = Integer.parseInt(parts[3]);
        return new Product(id, name, price, amount);
    };
    private static final Function<Product, String> productToStringMapper = product ->
            product.getName() + "|" + product.getPrice() + "|" + product.getAmount();

    @Override
    public List<Product> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAllByAmountGreaterThan(Integer lower) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getAmount() >= lower)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void save(Product product) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String productToSave = productToStringMapper.apply(product);
            bufferedWriter.write(productToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<String> findAllUniqueNames() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .map(Product::getName) // a -> a.method() -> A::method
                    .distinct()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public int findMinAmount() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .mapToInt(Product::getAmount)
                    .min()
                    .getAsInt();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public double findAverageAmount() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .mapToInt(Product::getAmount)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }



    @Override
    public Product findById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId() == id)
                    .findFirst()
                    .orElse(new Product(0,"",0.0,0));
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}

