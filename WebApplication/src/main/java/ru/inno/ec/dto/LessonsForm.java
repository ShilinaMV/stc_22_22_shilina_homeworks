package ru.inno.ec.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class LessonsForm {

    private String name;
    private String summary;
    //private LocalTime startTime;
    //private LocalTime finishTime;
}

