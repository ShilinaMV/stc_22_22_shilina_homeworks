package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;

import java.util.List;

@Repository
public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByStateNot(Lesson.State state);

    List<Lesson> findAllByCourseNullAndState(Lesson.State state);

    List<Lesson> findAllByCourse(Course course);

    List<Lesson> findAllByCourse_Id (Long id);

    List<Lesson> findAllByCourseIsNotContaining(Course course);

}