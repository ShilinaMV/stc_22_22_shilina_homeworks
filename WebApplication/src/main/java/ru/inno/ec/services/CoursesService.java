package ru.inno.ec.services;

import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.User;

import java.util.List;

public interface CoursesService {
    void addCourse(CourseForm course);

    List<Course> getAllCourses();

    void deleteCourse(Long id);

    void addStudentToCourse(Long courseId, Long studentId);

    Course getCourse(Long courseId);

    List<User> getNotInCoursesStudents(Long courseId);

    List<User> getInCoursesStudents(Long courseId);


}
