package ru.inno.ec.services;

import ru.inno.ec.dto.LessonsForm;
import ru.inno.ec.models.Lesson;
import java.util.List;

public interface LessonsService {


    void addLessonInCourse(Long lessonId, Long courseId);

    List<Lesson> findAll();

    List<Lesson> findAllByCourseIsNotContaining(Long id);

//    void addLesson(LessonsForm lesson);

    List<Lesson> findByCourse_Id(Long id);

    List<Lesson> getAllLessons();


    void addLessons(LessonsForm lesson);

    Lesson getLesson(Long Id);

    void updateLessons(Long lessonsId, LessonsForm lesson);

    void deleteLessons(Long Id);


}
