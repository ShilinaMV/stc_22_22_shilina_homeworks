package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.LessonsForm;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.Course;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.services.LessonsService;


import java.time.LocalTime;
import java.util.List;

@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;
    private final CoursesRepository coursesRepository;


    @Override
    public void addLessonInCourse(Long lessonId, Long courseId) {
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();
        Course course = coursesRepository.findById(courseId).orElseThrow();

        lesson.setCourse(course);

        lessonsRepository.save(lesson);
    }

    @Override
    public  List<Lesson> findAll() {
        return lessonsRepository.findAll();
    }

    @Override
    public List<Lesson> findAllByCourseIsNotContaining(Long id) {

        Course course = coursesRepository.findById(id).orElseThrow();

        return lessonsRepository.findAllByCourseIsNotContaining(course);
    }

    @Override
    public void addLessons(LessonsForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                //.startTime(lesson.getStartTime())
                //.finishTime(lesson.getFinishTime())
                .build();

        lessonsRepository.save(newLesson);

    }

    @Override
    public List<Lesson> findByCourse_Id(Long id) {
        return lessonsRepository.findAllByCourse_Id(id);
    }



    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAll();
    }

    @Override
    public void deleteLessons(Long id) {
        lessonsRepository.deleteById(id);
    }

    @Override
    public Lesson getLesson(Long id) {
        return lessonsRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateLessons(Long id, LessonsForm lessonsForm) {
        Lesson lesson = lessonsRepository.findById(id).orElseThrow();

        //lesson.setFinishTime(lessonsForm.getFinishTime());
        //lesson.setStartTime(lessonsForm.getStartTime());
        lesson.setName(lessonsForm.getName());
        lesson.setSummary(lessonsForm.getSummary());

        lessonsRepository.save(lesson);
    }
}

